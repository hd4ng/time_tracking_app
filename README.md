## Step 1: Break the app into components
* Add the file app-complete.js into index.html file. Run the app by using `npm start` to see how the app works.

* At first, we can separate the app into 2 components: `TimerList` and `Timer`. The first contains instances of the second.

* This list of timers has a little '+' icon at the bottom. We're able to add a new timers to the list using this button. So in reality, `TimerList` isn't just a list of timers. It also contains a widget to create new timer.
A component should, ideally, **only be responsible for one piece of functionality**. So the proper response here is for us to shrink `TimerList` back into its responsibility of just listing timer and nest it under a parent component. We will call the parent `TimersDashboard`. `TimersDashboard` will have `TimerList` and the '+' / create form Widget as children.

* '+' button has two representations. '+' button and create timer form (is showed when click the '+' button). We will create a parent component to control whether we render '+' button or a create form timer. We will call the parent `ToggleableTimerForm`.

* Timer also has 2 representations: timer's face and timer edit form. So we will create a parent component `EditableTimer` to control whether timer or timer edit form should rendered. And we will change the name `TimerList` to `EditableTimerList`.

* Since, the timer create form and timer edit form looks similar, we will use 1 component to render it: `TimerForm`.

* Finally, our app will break into component like:
- `TimersDashboard`
    - `EditableTimerList`
        - `EditableTimer`
            - `Timer`
            - `TimerForm`
    - `ToggleableTimerForm`
        - `TimerForm` (timer create form)
        - '+' button
---
## Step 2: Build a static version of the app
See app-1.js

---
## Step 3: Determine what should be state-ful

**TimersDashboard**
* `isOpen` boolean for ToggleableTimerForm.
**State-ful**. The data is defined here. It changes overtime. And it cannot be computed from other state or props.

**EditableTimerList**
* Timers properties. **State-full**. The data is defined in this component, changes overtime and cannot be computed from other state or props.

**EditableTimer**
* `editFormOpen` for the given timer. **State-ful**. The data is defined in this component, changes overtime and cannot be computed from other state or props.

**Timer**
* Timer properties. **Not state-full**. Properties are passed down from the parent.

State-ful data:
* The list of timers and properties of each timer
* Whether or not the edit form of a timer is open
* Whether or not the create form is open
---

## Step 4: Determine in which component each piece of state should live
* **Timer data** will be owned and managed by TimersDashboard
* Each Editable will manage the state of its **timer edit form**
* The ToggleableTimerForm will manage the state of its **form visibility**

---
## Step 5: Hard-code initial states

---

## Step 6: Add inverse data flow
We are going to need inverse data flow in two areas:
* TimerForm needs to propagate create and update events (create while under ToggleableTimerForm and update while under EditableForm). Both events will eventually reach TimersDashboard.
* Timer has a fair amount of behavior. It needs to handle **edit** and **delete** clicks, as well as the **start** and **stop** timer logic.